from rest_framework import serializers
from .models import Producto

# Create your tests here.
class ProductoSerializer(serializers.ModelSerializer):
    class Meta:
        model= Producto
        fields = '__all__'